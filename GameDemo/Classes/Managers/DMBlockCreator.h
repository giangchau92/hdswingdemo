//
//  DMBlockCreator.h
//  GameDemo
//
//  Created by Chau Giang on 3/27/16.
//  Copyright © 2016 Nguyen Giang Chau. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "DMBlock.h"

@interface DMBlockCreator : NSObject

+ (DMBlock*)creatorBlock;
+ (DMBlock*)createFirstBlock;
+ (CGPoint)getPositionWithLastBlock:(DMBlock*)block;

@end
