//
//  DMCamera.h
//  GameDemo
//
//  Created by Chau Giang on 3/27/16.
//  Copyright © 2016 Nguyen Giang Chau. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "cocos2d.h"

@class DMWorld;

typedef enum : NSUInteger {
    DMCameraStateFollowPlayer, // camera sẽ đi theo hàm update
    DMCameraStateNoControll // trạng thái này thì camera sẽ di chuyển theo CCAction hay tùy ý
} DMCameraState;

@interface DMCamera : CCNode

@property (nonatomic) DMCameraState state;

- (id)initWithWorld:(DMWorld*)world;

- (void)followPlayer;
- (void)gotoBegin;
- (void)gotoPreJump;
- (void)gotoStand2;

@end
