//
//  DMCamera.m
//  GameDemo
//
//  Created by Chau Giang on 3/27/16.
//  Copyright © 2016 Nguyen Giang Chau. All rights reserved.
//

#import "DMCamera.h"
#import "DMWorld.h"

#define TIME_MOVE 0.5
#define CAMERA_PADDING 20

@implementation DMCamera {
    DMWorld* _world;
    CGSize _size;
    CGPoint _currentScene;
}

- (id)initWithWorld:(DMWorld*)world {
    self = [super init];
    if (self) {
        _world = world;
        self.position = CGPointZero;
        _size = _world.contentSize;
    }
    return self;
}

- (void)onEnter {
    [super onEnter];
    [self schedule:@selector(_update:) interval:1/60.0];
}

- (void)onExit {
    [super onExit];
    [self unschedule:@selector(_update:)];
}

- (void)followPlayer {
    _currentScene = _position;
    _state = DMCameraStateFollowPlayer;
    // chỉ khi camera đi theo player thì mới cần dùng DMCamerastatusfollowplayer
}

- (void)gotoBegin {
    _state = DMCameraStateNoControll; // chuyển thành NoControll để mình có thể controll camera bằng Action hay set tay
    CCNode* player = _world.player;
    _position = CGPointMake(player.position.x + player.contentSize.width / 2 - _size.width / 2, 0);
    
}

- (void)gotoPreJump {
    _state = DMCameraStateNoControll;
    CGPoint playerPos = [_world getPlayerStartRight];
    CGRect stand2 = _world.blockTarget.boundingBox;
    NSInteger mid = (playerPos.x + CGRectGetMinX(stand2)) / 2;
    CCActionMoveTo* action = [CCActionMoveTo actionWithDuration:TIME_MOVE position:CGPointMake(mid - _size.width / 2, 0)];
    [self runAction:action];
    // chỉnh camera về vị trí chính giữa player và block target
}

- (void)gotoStand2 {
    _state = DMCameraStateNoControll;
    CCActionMoveTo* action = [CCActionMoveTo actionWithDuration:TIME_MOVE position:_currentScene];
    [self runAction:action];
    // khi player nhảy thì...
    // chỉnh camera về vị trí ban đầu
}

- (void)_update:(double)time {
    if (_state == DMCameraStateFollowPlayer) {
        CGRect player = _world.player.boundingBox;
        NSInteger min = CGRectGetMinX(player);
        NSInteger max = CGRectGetMaxX(player);
        if (min < self.position.x + CAMERA_PADDING)
            self.position = CGPointMake(player.origin.x - CAMERA_PADDING, 0);
        if (max > self.position.x + _size.width - CAMERA_PADDING)
            self.position = CGPointMake(max + CAMERA_PADDING - _size.width, 0);
    }
    // cập nhật content của world
    CCNode* content = _world.world;
    content.position = CGPointMake(- self.position.x, 0);
}

@end
