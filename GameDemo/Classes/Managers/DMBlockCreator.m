//
//  DMBlockCreator.m
//  GameDemo
//
//  Created by Chau Giang on 3/27/16.
//  Copyright © 2016 Nguyen Giang Chau. All rights reserved.
//

#import "DMBlockCreator.h"

#define BLOCK_HEIGHT 250
#define BLOCK_WIDTH_MIN 30
#define BLOCK_WIDTH_MAX 100

#define BLOCK_DISTANCE_MIN 20
#define BLOCK_DISTANCE_MAX 50

@implementation DMBlockCreator

+ (DMBlock*)creatorBlock {
    NSInteger width = BLOCK_WIDTH_MIN + random() % (BLOCK_WIDTH_MAX - BLOCK_WIDTH_MIN);
    DMBlock* block = [[DMBlock alloc] initBlockWithWidth:width height:BLOCK_HEIGHT];
    
    return block;
}

+ (DMBlock*)createFirstBlock {
    NSInteger width = 300;
    DMBlock* block = [[DMBlock alloc] initBlockWithWidth:width height:BLOCK_HEIGHT];
    
    return block;
}

+ (CGPoint)getPositionWithLastBlock:(DMBlock*)block {
    CGRect frame = [block boundingBox];
    NSInteger width = BLOCK_DISTANCE_MIN + random() % (BLOCK_DISTANCE_MAX - BLOCK_DISTANCE_MIN);
    return CGPointMake(CGRectGetMaxX(frame) + width, 0);
}

@end
