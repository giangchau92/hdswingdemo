//
//  HelloWorldScene.m
//
//  Created by : Nguyen Giang Chau
//  Project    : GameDemo
//  Date       : 3/25/16
//
//  Copyright (c) 2016 Nguyen Giang Chau.
//  All rights reserved.
//
// -----------------------------------------------------------------

#import "HelloWorldScene.h"
#import "DMWorld.h"

// -----------------------------------------------------------------------

@implementation HelloWorldScene {
    DMWorld* _world;
}

// -----------------------------------------------------------------------

- (id)init
{
    // Apple recommend assigning self with supers return value
    self = [super init];
    
    // The thing is, that if this fails, your app will 99.99% crash anyways, so why bother
    // Just make an assert, so that you can catch it in debug
    NSAssert(self, @"Whoops");
    
    // Background
    CCSprite9Slice *background = [CCSprite9Slice spriteWithImageNamed:@"white_square.png"];
    background.anchorPoint = CGPointZero;
    background.contentSize = [CCDirector sharedDirector].viewSize;
    background.color = [CCColor grayColor];
    [self addChild:background];
    
    _world = [[DMWorld alloc] initWithSize:self.contentSize];
    [self addChild:_world];
    
    self.userInteractionEnabled = YES;
    // done
    return self;
}

- (void)touchBegan:(CCTouch *)touch withEvent:(CCTouchEvent *)event {
    // bắt touch vào world
    [_world startJump];
}

// -----------------------------------------------------------------------

@end























// why not add a few extra lines, so we dont have to sit and edit at the bottom of the screen ...
