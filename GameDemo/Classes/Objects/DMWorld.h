//
//  DMWorld.h
//  GameDemo
//
//  Created by Chau Giang on 3/26/16.
//  Copyright © 2016 Nguyen Giang Chau. All rights reserved.
//

#import "cocos2d.h"
#import "DMPlayer.h"
#import "DMBlock.h"
#import "DMCamera.h"
#import "DMWorld.h"

@interface DMWorld : CCNode

@property (nonatomic, strong) CCPhysicsNode* world;
@property (nonatomic, strong) NSMutableArray* blockStands;
@property (nonatomic, strong) DMBlock* blockTarget;
@property (nonatomic, strong) DMPlayer* player;
@property (nonatomic, strong) DMCamera* camera;

- (id)initWithSize:(CGSize)size;

- (void)startJump;

- (CGPoint) getPlayerStartRight;

@end
