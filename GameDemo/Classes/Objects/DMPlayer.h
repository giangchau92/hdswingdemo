//
//  DMPlayer.h
//  GameDemo
//
//  Created by Chau Giang on 3/26/16.
//  Copyright © 2016 Nguyen Giang Chau. All rights reserved.
//

#import "cocos2d.h"

@class DMWorld;

@interface DMPlayer : CCNode

@property (weak, nonatomic) DMWorld* world;

- (void)startJump;
- (CGPoint)getRopePoint;
- (void)nextJump;
- (void)preJump;

@end
