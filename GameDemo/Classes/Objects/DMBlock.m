//
//  DMBlock.m
//  GameDemo
//
//  Created by Chau Giang on 3/26/16.
//  Copyright © 2016 Nguyen Giang Chau. All rights reserved.
//

#import "DMBlock.h"

@implementation DMBlock {
    CCPhysicsBody* _physicBK;
}

- (id)initBlockWithWidth:(NSInteger)width height:(NSInteger)height {
    self = [super init];
    if (self) {
        //
        self.contentSize = CGSizeMake(width, height);
        // vẽ một cái node để mọi người đễ hình dung cái Node bound khi physicbody bị mất
        CCDrawNode* _drawNode = [CCDrawNode new];
        _drawNode.position = CGPointZero;
        _drawNode.contentSize = self.contentSize;
        [self addChild:_drawNode];
        [_drawNode drawSegmentFrom:CGPointMake(0, 0) to:CGPointMake(width, 0) radius:0.5 color:[CCColor colorWithRed:0.5 green:0 blue:0]];
        [_drawNode drawSegmentFrom:CGPointMake(0, 0) to:CGPointMake(0, height) radius:0.5 color:[CCColor colorWithRed:0.5 green:0 blue:0]];
        [_drawNode drawSegmentFrom:CGPointMake(0, height) to:CGPointMake(width, height) radius:0.5 color:[CCColor colorWithRed:0.5 green:0 blue:0]];
        [_drawNode drawSegmentFrom:CGPointMake(width, 0) to:CGPointMake(width, height) radius:0.5 color:[CCColor colorWithRed:0.5 green:0 blue:0]];
        // cài đặt physicbody
        CCPhysicsBody* physic = [CCPhysicsBody bodyWithRect:CGRectMake(0, 0, width, height) cornerRadius:0];
        physic.type = CCPhysicsBodyTypeStatic; // đối tượng tĩnh, ko đi đâu đc hết
        physic.collisionType = NSStringFromClass([self class]);
        self.physicsBody = physic;
    }
    return self;
}

- (CGPoint)getPlayerStartPosition {
    CGRect bound = self.boundingBox;
    return CGPointMake(CGRectGetMaxX(bound), CGRectGetMaxY(bound));
}

- (void)enablePhysics {
    if (_physicBK)
        self.physicsBody = _physicBK;
}

- (void)disablePhysics {
    _physicBK = self.physicsBody;
    self.physicsBody = nil;
}

@end
