//
//  DMBlock.h
//  GameDemo
//
//  Created by Chau Giang on 3/26/16.
//  Copyright © 2016 Nguyen Giang Chau. All rights reserved.
//

#import "cocos2d.h"

@interface DMBlock : CCNode

- (id)initBlockWithWidth:(NSInteger)width height:(NSInteger)height;

- (CGPoint)getPlayerStartPosition;

- (void)enablePhysics;

- (void)disablePhysics;


@end
