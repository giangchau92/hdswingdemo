//
//  DMWorld.m
//  GameDemo
//
//  Created by Chau Giang on 3/26/16.
//  Copyright © 2016 Nguyen Giang Chau. All rights reserved.
//


/*----------------------------------------------------------*/
//                  FLOW CỦA GAME
//
//      WORLD -> INIT -> PRE-FIRST -> PRE-JUMP
//                                   _   |
//                             ______/|  |
//                            /          |
//                   NEXT-JUMP <--   START-JUMP
//
//
/*----------------------------------------------------------*/

#import "DMWorld.h"
#import "DMBlockCreator.h"
#import "HelloWorldScene.h"

#define COLLISION_EPSILONE 3

@interface DMWorld ()<CCPhysicsCollisionDelegate>

@end

@implementation DMWorld {
    CCNode* _handleNode;
    BOOL _isJumping;
    BOOL _handleTouch;
    BOOL _isOut; // check coi player có bay ra ngoài để thua ko
    NSUInteger _jumpFlag;
}

- (id)initWithSize:(CGSize)size {
    self = [super init];
    if (self) {
        self.contentSize = size;
        _isJumping = NO;
        _handleTouch = NO;
        _blockStands = [NSMutableArray new];
        // init physic node work
        _world = [CCPhysicsNode new];
        _world.contentSize = size;
        _world.gravity = CGPointMake(0, -980);
        _world.debugDraw = YES;
        _world.collisionDelegate = self;
        [self addChild:_world];
        
        // cài đặt camera
        _camera = [[DMCamera alloc] initWithWorld:self];
        [self addChild:_camera];
    }
    return self;
}

- (void)onEnter {
    [super onEnter];
    [self schedule:@selector(_update:) interval:1/60.0];
    [self _initGame];
}

- (void)onExit {
    [super onExit];
    [self unschedule:@selector(_update:)];
}

- (void)_initGame {
    // create first block
    DMBlock* block = [DMBlockCreator createFirstBlock];
    block.position = CGPointZero;
    [_world addChild:block];
    [_blockStands addObject:block];
    // player create
    CGPoint pos = [block getPlayerStartPosition];
    _player = [DMPlayer new];
    _player.world = self;
    _player.position = CGPointMake(pos.x - _player.contentSize.width, pos.y);
    [_world addChild:_player];
    
    // move camera
    [_camera gotoBegin];
    
    [self scheduleOnce:@selector(_worldPreFirst) delay:0.5];
}

- (void)_worldPreFirst {
    // born new block
    [self _bornGroundTarget];
    // move camera
    [_camera gotoPreJump];
    // preJump
    [self scheduleOnce:@selector(_worldPreJump) delay:2];
}

- (void)_worldPreJump {
    _isOut = NO;
    _handleTouch = YES;
    [_player preJump];
    [_camera followPlayer];
}

- (void)_worldStartJump {
    _isJumping = YES;
    _handleTouch = NO;
    _jumpFlag = 1 << 0;
    [self _makeRope];
    [self _disableBlockStand]; // disable thành phần physic cho block để player có thể bay xuyên qua
    // player
    [_player startJump];
    // camera
    [_camera gotoStand2];
}

- (void)_worldNextJump {
    _isJumping = NO;
    // born block
    [self _bornGroundTarget];
    // player next jump
    [_player nextJump];
    // move camera
    [_camera gotoPreJump];
    // preJump
    [self scheduleOnce:@selector(_worldPreJump) delay:2];
}


/*
 * Sinh block mới, animation nhập block nếu có
 */
- (void)_bornGroundTarget {
    CGRect lastBlockRect = [_blockStands.lastObject boundingBox];
    if (_blockTarget) {
        // nhập với mấy block kia
        
        CCActionMoveTo* action = [CCActionMoveTo actionWithDuration:0.5 position:CGPointMake(CGRectGetMaxX(lastBlockRect), 0)];
        [_blockTarget runAction:action];
        [_blockStands addObject:_blockTarget];
        _blockTarget = nil;
        [self _enableBlockStand];
    }
    
    // show new block
    DMBlock* block = [DMBlockCreator creatorBlock];
    block.position = [DMBlockCreator getPositionWithLastBlock:_blockStands.lastObject];
    [_world addChild:block];
    _blockTarget = block;
}

- (void)_update:(double)time {
    // update bắt trạng thái của player
    if (_isJumping) {
        // kiểm tra velocity để bắt khi player đu ngược trở lại thì enable lại physical cho những block đã bị disablePhysic
        // y âm -> dương -> âm`
        // 1 << 0 -> âm
        // 1 << 1 -> dương
        // 1 << 2 -> âm
        //
        CGPoint vel = _player.physicsBody.velocity;
        // lúc đi xuống rồi đi lên
        if (vel.y > 0 && _jumpFlag == (1 << 0) && !(_jumpFlag & (1 << 1))) {
            _jumpFlag |= (1 << 1);
        }
        // đi lên sau đó đi xuống
        if (vel.y < 0 && _jumpFlag & (1 << 1) && !(_jumpFlag & (1 << 2))) {
            _jumpFlag |=  (1 << 2);
            // appear disaapear block
            [self _enableBlockStand];
        }
            
    } else {
        // kiểm tra player đi ngược ra ngoài block thì thua
        CGRect playerBound = [_player boundingBox];
        CGRect blockBound = [[_blockStands firstObject] boundingBox];
        if (CGRectGetMaxY(playerBound) < CGRectGetMaxY(blockBound) && !_isOut) {
            _isOut = YES;
            [self goReplay];
        }
        
    }
}


/* Tạo dây nối player và một node đc neo ở block */
- (void)_makeRope {
    // tạo một Node Physic tạm (static) để nối player với nó
    CGRect rect = [_blockStands.lastObject boundingBox];
    _handleNode = [[CCNode alloc] init];
    _handleNode.contentSize = CGSizeMake(1, 1);
    _handleNode.position = CGPointMake(CGRectGetMaxX(rect), CGRectGetMaxY(rect));
    _handleNode.physicsBody = [CCPhysicsBody bodyWithRect:CGRectMake(0, 0, 1, 1) cornerRadius:0];
    _handleNode.physicsBody.type = CCPhysicsBodyTypeStatic;
    [_world addChild:_handleNode];
    
    [CCPhysicsJoint connectedDistanceJointWithBodyA:_player.physicsBody  bodyB:_handleNode.physicsBody anchorA:[_player getRopePoint] anchorB:CGPointZero];
}

- (void)_disableBlockStand {
    [_blockStands makeObjectsPerformSelector:@selector(disablePhysics)];
}

- (void)_enableBlockStand {
    [_blockStands makeObjectsPerformSelector:@selector(enablePhysics)];
}

- (void)_onPlayerCollionWithBlock:(DMBlock*)block {
    // delete rope
    [_player.physicsBody.joints.firstObject invalidate];
    [_handleNode removeFromParent];
    // kiểm tra nếu là nếu player nằm trên block target thì ok
    if (block == _blockTarget) {
        double blockMax = CGRectGetMaxY(block.boundingBox);
        double playerMin = CGRectGetMinY(_player.boundingBox);
        if (playerMin + COLLISION_EPSILONE > blockMax) {
            NSLog(@"PASSED");
            [self _worldNextJump];
            return;
        }
    }
    // mọi trường hợp khác đều faild nhé
    NSLog(@"FAILD");
    [self goReplay];
}

// có thể animation của block khi gộp block chưa xong
// nên cần hàm này để lấy vị trí khi animation õng
- (CGPoint) getPlayerStartRight {
    double x = 0;
    for (int i = 0; i < _blockStands.count; i++) {
        DMBlock* block = [_blockStands objectAtIndex:i];
        x += block.boundingBox.size.width;
    }
    return CGPointMake(x, [_blockStands.firstObject boundingBox].size.height);
}

- (void)goReplay {
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [[CCDirectorIOS sharedDirector] replaceScene:[HelloWorldScene new] withTransition:[CCTransition transitionFadeWithColor:[CCColor colorWithRed:1 green:1 blue:1] duration:1]];
    });
}


// hàm callback khi 1 DMBlock va cham với một DMPlayer
// return YES nếu muốn chipmuck phản hồi va chạm
// return NO nếu muốn chipmunk bỏ qua nó, tức nó sẽ đi xuyên qua nhau
-(BOOL)ccPhysicsCollisionBegin:(CCPhysicsCollisionPair *)pair DMBlock:(CCNode *)nodeA DMPlayer:(CCNode *)nodeB {
    if (_isJumping == NO) // chỉ xét lúc đang nhảy thôi
        return YES; // còn khi chưa nhảy thì phần phản hồi va chạm bình thường
    else {
        CGPoint vel = nodeB.physicsBody.velocity;
        if (vel.y > 0)
            return NO;
        else {
            [self _onPlayerCollionWithBlock:(DMBlock*)nodeA];
            return YES;
        }
    }
}

#pragma mark CALL OUTSIDE

- (void)startJump {
    if (_handleTouch) {
        [self _worldStartJump];
    }
}



@end
