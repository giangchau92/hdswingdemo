//
//  DMPlayer.m
//  GameDemo
//
//  Created by Chau Giang on 3/26/16.
//  Copyright © 2016 Nguyen Giang Chau. All rights reserved.
//

#import "DMPlayer.h"
#import "DMWorld.h"

#define PLAYER_VELOCITY -50

@implementation DMPlayer {
    CGPoint _velocity;
}

- (id)init {
    self = [super init];
    if (self) {
        self.contentSize = CGSizeMake(20, 40);
        _velocity = CGPointZero;
        CCPhysicsBody* physic = [CCPhysicsBody bodyWithRect:CGRectMake(0, 0, self.contentSize.width, self.contentSize.height) cornerRadius:0];
        physic.type = CCPhysicsBodyTypeDynamic;
        physic.collisionType = NSStringFromClass([self class]);
        physic.allowsRotation = NO;
        physic.mass = 10;
        self.physicsBody = physic;
    }
    return self;
}

- (void)nextJump {
    NSAssert(_world, @"World null");
    // chuyển physicbody về static để có thể CCAction mà ko bị sai
    self.physicsBody.type = CCPhysicsBodyTypeStatic;
    // lúc này player và block nó đứng lên ko liên quan đến nhau
    // di chuyển 1 cách độ lập
    // move player to start point
    CGPoint pos = [_world getPlayerStartRight];
    pos = CGPointMake(pos.x - self.boundingBox.size.width, pos.y);
    CCActionMoveTo* action = [CCActionMoveTo actionWithDuration:0.5 position:pos];
    [self runAction:action];
}

- (void)preJump {
    // set vận tốc lùi cho player rồi update
    self.physicsBody.type = CCPhysicsBodyTypeDynamic;
    self.physicsBody.allowsRotation = NO;
    _velocity = CGPointMake(PLAYER_VELOCITY, 0);
    [self schedule:@selector(_update:) interval:1/60.0];
}

- (void)startJump {
    // set lại vận tốc player thành 0
    _velocity = CGPointZero;
    [self unschedule:@selector(_update:)];
    // thêm ít lực để player có thể bay cao hơn block target
    [self.physicsBody applyForce:CGPointMake(0, -100000)];
}

- (void)_update:(double)time {
    double x = self.position.x + time * _velocity.x;
    double y = self.position.y + time * _velocity.y;
    self.position = CGPointMake(x, y);
}

// trả về anchorpoint điểm sợi dây đính vào
- (CGPoint)getRopePoint {
    return CGPointMake(self.contentSize.width / 2, self.contentSize.height / 2);
}

@end
